var assert = require('chai').assert,
    request = require('supertest'),
    expect = require('chai').expect;

var url = 'https://graph.facebook.com/v2.12/1269182809849975';
var token = 'Bearer EAACcllC7sEQBADYJOeWmAZAGOjkynRySoACFOdBZAZC65FqfX5RnLbonGKZCionTcUtwq0n067eKEZANx0ql9mYzG5xIpqVi5YVa5v0H7ocLEmXcHnkE6Q9ByeE8QnG8HxAZCXqHqzTSeEEzJMVZAp6WQoNqnZAZCgvZAdbhyyRtaZB3QZDZD'
var text = 'This+is+a+test+message'
var new_text = 'This+is+vai+curinthia'

describe('Testando API do Facebook', function() {

  it('Realizando um Post no mural', function(done) {
    request(url)
      .post('/feed?message=' + text)
      .set('Content-Type', 'application/json')
      .set('authorization', token)
      .expect(200);
      done();
  });

  it('Alterando o Post no mural', function(done) {
    request(url)
      .post('_1269230413178548?message=' + new_text)
      .set('Content-Type', 'application/json')
      .set('authorization', token)
      .expect(200);
      done();
  });

  it('Todos os posts publicados', function(done) {
    request(url)
      .get('/posts')
      .set('Content-Type', 'application/json')
      .set('authorization', token)
      .expect(200);
      done();
  });
  
});
